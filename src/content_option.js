const logotext = "KOTHARU SITARAM PHANI BHUSHAN";
const meta = {
  title: "PHANI BHUSHAN",
  description:
    "I’m K.S.R PHANI BHUSHAN, a full stack developer and a freelancer from India. I have a passion for web design and love to create for web and mobile devices.",
};

const introdata = {
  title: "Hey I’m K.S.R PHANI BHUSHAN",
  animated: {
    first: "Experienced Software Engineer",
    second: "I'm a Freelancer",
    third: "I'm a React Native Developer",
  },
  description:
    "I'm a Passionate Software Engineer, Specializing in Web and Mobile Development, Dedicated to Delivering Innovative Solutions and Fostering Collaboration.",
  your_img_url:
    "https://www.pandasecurity.com/en/mediacenter/src/uploads/2016/03/pandasecurity-Who-are-the-most-famous-hackers-in-history.jpg",
};

const dataabout = {
  title: "A Bit About My Self",
  aboutme:
    "Experienced Software Engineer with a Strong Foundation in Web and Mobile Application Development. Proficient in HTML, CSS, React.Js, React-Native, TypeScript, JavaScript, and a range of Front-End Technologies. Basic Knowledge of Next.Js and Nest.Js for Full-Stack Development. Possesses a Moderated-Level of DevOps skills, Including writing YAML codes, creating Helm Charts and Maintaining EC2 instances with AWS. Skilled in Kubernetes, capable of Managing Clusters, Docker containers, and Pods, as well as creating and Managing Namespaces. Experienced in Custom-Debian Operating Systems and Linux environments, leveraging tools like VirtualBox, Boxes, Rancher, OpenLens, K9s and DigitalOcean for Efficient DevOps Processes. Committed to Delivering High-Quality Solutions On Time, Fostering Collaboration and Staying Updated with Industry Trends.",
};

const skills = [
  {
    name: "HTML",
    value: 90,
  },
  {
    name: "CSS",
    value: 85,
  },
  {
    name: "Javascript",
    value: 80,
  },
  {
    name: "Typescript",
    value: 90,
  },
  {
    name: "React",
    value: 60,
  },
  {
    name: "React Native",
    value: 85,
  },
  {
    name: "Kubernetes",
    value: 70,
  },
  {
    name: "Next.js",
    value: 70,
  },
  {
    name: "Nest.js",
    value: 70,
  },
];

const services = [
  {
    title: "UI & UX Design",
    description:
      "I specialize in crafting user-centric digital experiences, combining aesthetics with intuitive functionality. From wireframing to responsive web and mobile design, my expertise ensures that every interaction is seamless and visually appealing. I'm dedicated to enhancing user satisfaction through thoughtful, beautiful design.",
  },
  {
    title: "Mobile Apps",
    description:
      "In the realm of mobile app development, I leverage my skills to bring innovative ideas to life on both iOS and Android platforms.",
  },
];

const dataportfolio = [
  {
    img: "https://picsum.photos/400/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/800/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/600/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/300/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/700/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },

  {
    img: "https://picsum.photos/400/600/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/300/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/550/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
  {
    img: "https://picsum.photos/400/700/?grayscale",
    description:
      "The wisdom of life consists in the elimination of non-essentials.",
    link: "#",
  },
];

const contactConfig = {
  YOUR_EMAIL: "kotharuphani@gmail.com",
  description:
    "Experienced software engineer skilled in web and mobile development. Committed to delivering excellence through innovation and collaboration. ",
  YOUR_SERVICE_ID: "service_id",
  YOUR_TEMPLATE_ID: "template_id",
  YOUR_USER_ID: "user_id",
};

const socialprofils = {
  github: "https://github.com/Phani2525",
  linkedin: "https://www.linkedin.com/in/phani-bhushan-131b1327a/",
  gitlab: "https://gitlab.com/phani25",
  discord: "https://discord.gg/phanibhushan#2525",
};
export {
  meta,
  dataabout,
  dataportfolio,
  skills,
  services,
  introdata,
  contactConfig,
  socialprofils,
  logotext,
};
