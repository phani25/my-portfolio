import React from "react";
import "./style.css";
import { FaGithub, FaLinkedin, FaGitlab, FaDiscord } from "react-icons/fa";
import { socialprofils } from "../../content_option";

export const Socialicons = (params) => {
  const iconSize = "2em"; // Adjust the size as needed
  const spacing = "1em"; // Adjust the spacing as needed
  const followMeSize = "1.5em"; // Adjust the "Follow Me" text size as needed

  return (
    <div className="stick_follow_icon">
      <h4
        style={{
          fontSize: followMeSize,
          fontStyle: "italic",
          marginBottom: spacing,
        }}
      >
        Reach Me On
      </h4>
      <ul>
        {socialprofils.github && (
          <li>
            <a href={socialprofils.github}>
              <FaGithub style={{ fontSize: iconSize, marginBottom: spacing }} />
            </a>
          </li>
        )}
        {socialprofils.linkedin && (
          <li>
            <a href={socialprofils.linkedin}>
              <FaLinkedin
                style={{ fontSize: iconSize, marginBottom: spacing }}
              />
            </a>
          </li>
        )}
        {socialprofils.gitlab && (
          <li>
            <a href={socialprofils.gitlab}>
              <FaGitlab style={{ fontSize: iconSize, marginBottom: spacing }} />
            </a>
          </li>
        )}
        {socialprofils.discord && (
          <li>
            <a href={socialprofils.discord}>
              <FaDiscord
                style={{ fontSize: iconSize, marginBottom: spacing }}
              />
            </a>
          </li>
        )}
      </ul>
    </div>
  );
};
